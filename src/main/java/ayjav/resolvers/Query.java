package ayjav.resolvers;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import ayjav.model.Message;
import ayjav.model.User;
import ayjav.model.Channel;
import ayjav.service.MessageRepository;
import ayjav.service.UserService;
import ayjav.service.ChannelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class Query implements GraphQLQueryResolver {


    @Autowired
    private ChannelService channelService;

    @Autowired
    private UserService userService;

    @Autowired
    private MessageRepository messageRepository;

    public List<User> users(final InputUser filter) {
        return userService.find(InputUser.convert(filter));
    }

    public Optional<User> user(final Long id) {
        return userService.findById(id);
    }

    public Optional<Channel> channel(final Long id) {
        return channelService.findById(id);
    }

    public Optional<Message> message(final Long id) {
        return messageRepository.findById(id);
    }

    public List<Channel> channels(final InputChannel filter) {
        return channelService.find(InputChannel.convert(filter));
    }

    public MessagePageableResponse messages(final InputPage inputPage) {
        Page<Message> messages = messageRepository.findAll(InputPage.convert(inputPage));
        return new MessagePageableResponse(messages);
    }
}
