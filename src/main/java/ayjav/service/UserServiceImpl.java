package ayjav.service;

import ayjav.model.User;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> find(final User person) {
        if (person == null) {
            return userRepository.findAll();
        }

        ExampleMatcher matcher = ExampleMatcher.matching()
                .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING)
                .withIgnoreCase();

        return StreamSupport.stream(userRepository.findAll(Example.of(person, matcher)).spliterator(), false)
                .collect(Collectors.toList());
    }

    public Optional<User> findById(final Long id) {
        return userRepository.findById(id);
    }

    public User save(User person) {
        return this.userRepository.save(person);
    }

}
