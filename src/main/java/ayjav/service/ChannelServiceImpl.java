package ayjav.service;

import ayjav.model.Channel;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@Transactional
public class ChannelServiceImpl implements ChannelService {

    private ChannelRepository channelRepository;

    public ChannelServiceImpl(ChannelRepository channelRepository) {
        this.channelRepository = channelRepository;
    }

    public List<Channel> find(final Channel channel) {
        if (channel == null) {
            return channelRepository.findAll();
        }

        ExampleMatcher matcher = ExampleMatcher.matching()
                .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING)
                .withIgnoreCase();

        return StreamSupport.stream(channelRepository.findAll(Example.of(channel, matcher)).spliterator(), false)
                .collect(Collectors.toList());
    }

    public Optional<Channel> findById(final Long id) {
        return channelRepository.findById(id);
    }

    public Channel save(Channel channel) {
        return this.channelRepository.save(channel);
    }

}