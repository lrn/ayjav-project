package ayjav.resolvers;


import com.coxautodev.graphql.tools.GraphQLSubscriptionResolver;
import ayjav.model.Message;
import ayjav.publishers.MessagePublisher;
import org.reactivestreams.Publisher;
import org.springframework.stereotype.Component;

@Component
class Subscription implements GraphQLSubscriptionResolver {

    private MessagePublisher messagePublisher;

    Subscription(MessagePublisher messagePublisher) {
        this.messagePublisher = messagePublisher;
    }

    Publisher<Message> messages() {
        return messagePublisher.getPublisher();
    }
}
