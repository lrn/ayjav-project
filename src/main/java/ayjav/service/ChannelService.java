package ayjav.service;

import ayjav.model.Channel;

import java.util.List;
import java.util.Optional;

public interface ChannelService {

    List<Channel> find(Channel channel);

    Optional<Channel> findById(Long id);

    Channel save(Channel channel);
}