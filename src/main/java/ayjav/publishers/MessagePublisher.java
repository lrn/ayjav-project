package ayjav.publishers;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.observables.ConnectableObservable;
import lombok.extern.slf4j.Slf4j;
import ayjav.model.Message;
import ayjav.service.ChannelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class MessagePublisher {


    @Autowired
    private ChannelService channelService;

    private final Flowable<Message> publisher;

    private ObservableEmitter<Message> emitter;

    public MessagePublisher() {
        Observable<Message> messageUpdateObservable = Observable.create(emitter -> {
            this.emitter = emitter;
        });

        ConnectableObservable<Message> connectableObservable = messageUpdateObservable.share().publish();
        connectableObservable.connect();


        publisher = connectableObservable.toFlowable(BackpressureStrategy.BUFFER);
    }

    public void publish(final Message message) {
        emitter.onNext(message);
    }

    public Flowable<Message> getPublisher() {
        return this.publisher;
    }

}
