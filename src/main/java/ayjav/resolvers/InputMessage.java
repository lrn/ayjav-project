package ayjav.resolvers;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InputMessage {

    private Long channelId;
    private Long UserId;
    private String message;
}
