package ayjav.service;

import ayjav.model.Channel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChannelRepository extends CrudRepository<Channel, Long>, QueryByExampleExecutor<Channel> {

    List<Channel> findAll();
}
