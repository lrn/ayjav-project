package ayjav;

import ayjav.dataloader.CsvReader;
import ayjav.dataloader.CsvChannel;
import ayjav.resolvers.InputUser;
import ayjav.service.UserService;
import ayjav.service.ChannelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;


@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {


    @Autowired
    private UserService userService;

    @Autowired
    private ChannelService channelService;

    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {

        try {
            List<InputUser> users = CsvReader.loadObjectList(InputUser.class, "csv/users.csv");
            for (InputUser user : users) {
                userService.save(InputUser.convert(user));
            }

            List<CsvChannel> channels = CsvReader.loadObjectList(CsvChannel.class, "csv/channels.csv");
            for (CsvChannel channel : channels) {
                channelService.save(channel.convert(userService));
            }
        } catch (IOException e) {
            System.out.println("Error loading data: " + e.getMessage());
        }
    }

}
