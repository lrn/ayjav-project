# Instant messeenger

* course: AYJAV

## About

* GraphQL backend for instant messenger implemented in JAVA.
* Framework Spring
* Docker used for deployment
* Storage mocked by CSV files in resources, project is using data layer to easily add persistant layer.
* libraries managed by gradle and listed in related `gradle/` folder
  
## Structure

* model represents data layer and relation between objects- Channel, Message, User
* publisher implements subscriptions
* resolvers implements graphql queries and mutations using services
* services defines way of searching and manipulation data models

## Running locally

1. build image (in root directery)
`docker build -t im-ayjav-java .`

2. run backend application on your localhost, port 4000
`docker run --rm --name im-ayjav-java -p 4000:8080 im-ayjav-java`

3. open graphical interface for running graphql queries, mutations or subscriptions

   * http://localhost:4000/graphiql
   * all available queries are listed in this document `src/main/resources/ayjav.graphqls`


## Examples:

1. query all channels
```
{
  channels {
    name
    users {
      id
    }
  }
}
```

1. create mew message
```
mutation {
  addMessage(message: {message: "Good morning! Welcome to AYJAV project", userId: 1, channelId: 6}) {
    id
    message
  }
}
```

3. query all messages
```
{
  messages {
    content {
      id
      message
      createdOn
      user {
        username
      }
    }
    pageInfo {
      totalPages
    }
  }
}
```

4. get all users
```
{
  users {
    id
    username
    fullname
  }
}
```

5. New users or channels can be created accordingly
