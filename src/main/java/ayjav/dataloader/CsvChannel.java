package ayjav.dataloader;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ayjav.model.User;
import ayjav.model.Channel;
import ayjav.service.UserService;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CsvChannel {

    private String name;
    private String topic;
    private String users;

    public Channel convert(UserService userService) {

        List<User> usersList = new ArrayList<>();
        for (String username : StringUtils.split(users, ",")) {
            usersList.addAll(userService.find(new User(username, null, null)));
        }

        return new Channel(this.name, this.topic, usersList);

    }
}
