package ayjav.resolvers;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ayjav.model.User;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InputUser {

    private String username;
    private String fullname;

    User convert() {
        return convert(this);
    }

    public static User convert(InputUser user) {
        return user != null ? new User(user.username, user.fullname, null) : null;
    }
}
