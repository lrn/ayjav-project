package ayjav.resolvers;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import ayjav.model.Message;
import ayjav.model.User;
import ayjav.model.Channel;
import ayjav.publishers.MessagePublisher;
import ayjav.service.MessageRepository;
import ayjav.service.UserRepository;
import ayjav.service.ChannelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.util.Optional;

@Component
public class Mutation implements GraphQLMutationResolver {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ChannelRepository channelRepository;

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private MessagePublisher messagePublisher;

    public User addUser(final InputUser user) {
        return this.userRepository.save(user.convert());
    }

    public Channel addChannel(final InputChannel channel) {
        return this.channelRepository.save(channel.convert());
    }

    public Channel addUserToChannel(final Long channelId, final Long userId) {
        Channel channel = channelRepository.findById(channelId).get();
        channel.getUsers().add(userRepository.findById(userId).get());
        return channelRepository.save(channel);
    }

    public Message addMessage(final InputMessage message) {
        Message result = null;
        User user = userRepository.findById(message.getUserId()).get();
        Optional<Channel> channel = channelRepository.findById(message.getChannelId());

        if (channel.isPresent()) {
            result = messageRepository.save(new Message(message.getMessage(), ZonedDateTime.now(), user, channel.get()));
            messagePublisher.publish(result);
        }

        return result;
    }

}
