package ayjav.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false, exclude = {"channels"})
public class User extends BaseEntity {

    private String username;
    private String fullname;

    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "users")
    private List<Channel> channels;

}
