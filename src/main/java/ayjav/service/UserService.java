package ayjav.service;

import ayjav.model.User;

import java.util.List;
import java.util.Optional;

public interface UserService {


    List<User> find(User user);

    Optional<User> findById(Long id);

    User save(User user);
}