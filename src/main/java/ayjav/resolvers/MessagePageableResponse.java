package ayjav.resolvers;

import lombok.Data;
import ayjav.model.Message;
import org.springframework.data.domain.Page;

import java.util.List;

@Data
public class MessagePageableResponse extends PageableResponse {

    private List<Message> content;

    public MessagePageableResponse(Page<Message> page) {
        super(page);
        this.content = page.getContent();
    }
}
