package ayjav.resolvers;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ayjav.model.Channel;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InputChannel {

    private String name;
    private String topic;

    Channel convert() {
        return convert(this);
    }

    static Channel convert(InputChannel channel) {
        return channel != null ? new Channel(channel.getName(), channel.getTopic(), null) : null;
    }
}
